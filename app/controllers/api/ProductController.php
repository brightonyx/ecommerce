<?php
/**
 * API controller for product CRUD
 *
 * @author  Paul Brighton
 * @package controllers/api
 */

namespace API\Controllers;

use API\Controllers\Base\Controller;
use Entities\Util;
use Models\Inventory;
use Models\Product;
use Services\AuthService;

class ProductController extends Controller {

	/**
	 * Adds a new product
	 *
	 */
	public function addAction(): void {
		// Make sure the request is done via POST and AJAX
		if ($this->request->isPost() and $this->request->isAjax()) {
			// Get the JSON input
			if ($input = $this->request->getJsonRawBody(true)) {
				// Handle the save logic
				$this->save($input);
			} else {
				Util::json_error('Invalid input format');
			}
		} else {
			// Deliberately give an ambiguous error message to avoid disclosing our accepted types
			Util::json_error('Invalid request format');
		}
	}

	/**
	 * Updates an existing product
	 *
	 * @param int|null $id
	 */
	public function editAction($id = null): void {
		// Make sure the request is done via POST and AJAX
		if ($this->request->isPut() and $this->request->isAjax()) {
			// Get the JSON input
			if ($input = $this->request->getJsonRawBody(true)) {
				// Handle the save logic
				$this->save($input, $id);
			} else {
				Util::json_error('Invalid input format');
			}
		} else {
			// Deliberately give an ambiguous error message to avoid disclosing our accepted types
			Util::json_error('Invalid request format');
		}
	}

	/**
	 * Deletes an existing product/inventory
	 *
	 * @param int|null $id
	 * @throws \Phalcon\Mvc\Collection\Exception
	 */
	public function deleteAction($id = null): void {
		// Make sure the request is done via POST and AJAX
		if ($this->request->isDelete() and $this->request->isAjax()) {
			// Make sure the ID is valid
			if ($inventory = Inventory::find_by_id($id)) {
				$references = ' and inventory';
				// If this inventory is linked to any orders, delete them first
				if ($inventory->orders->delete()) {
					$references = ', inventory and orders';
				}
				$product = $inventory->product;

				// Now, delete the inventory and the product will be deleted by the CASCADE rule
				if (!$inventory->delete()) {
					$messages = $inventory->getMessages();
					foreach ($messages as $message) {
						// Add all errors to the API response object
						$this->api_response->add_error($message->getMessage());
					}
				} else {
					// Finally, delete the actual product
					if (!$product->delete()) {
						$messages = $product->getMessages();
						foreach ($messages as $message) {
							// Add all errors to the API response object
							$this->api_response->add_error($message->getMessage());
						}
					}
					$this->api_response->message = "Product{$references} were deleted successfully";
				}
				Util::json_response($this->api_response, null, '/product/list');
			}
		} else {
			// Deliberately give an ambiguous error message to avoid disclosing our accepted types
			Util::json_error('Invalid request format');
		}
	}

	/**
	 * Saves the product in the database
	 * Used for both add and update
	 * TODO move this to a product service
	 *
	 * @param array $input
	 * @param int|null $id
	 * @return bool
	 */
	private function save(array $input, $id = null): bool {
		if (!$sku = Util::arval($input, 'sku')) {
			Util::json_error('Missing product SKU');
		} elseif (!$name = Util::arval($input, 'name')) {
			Util::json_error('Missing product name');
		} elseif (!$price = Util::arval($input, 'price')) {
			Util::json_error('Missing product price');
		} else {
			$user = AuthService::get_loggedin_user();
			// Get the inventory by ID
			if (!$inventory = Inventory::find_by_id($id)) {
				// Nothing found, create a new inventory and a product
				$inventory = new Inventory();
				$product   = new Product();
				$status    = 'created';
			} else {
				$product = $inventory->product;
				$status  = 'updated';
			}
			// Check if a user owns this product (for edits only)
			if ($product->id and $product->admin_id !== $user['id']) {
				Util::json_error('You are unauthorized to edit this product!');
			} else {
				$product->admin_id = $user['id'];
				$product->name = $name;
				$product->type = Util::arval($input, 'type');
				$product->description = Util::arval($input, 'description');
				$product->style = Util::arval($input, 'style');
				$product->brand = Util::arval($input, 'brand');
				$product->url = Util::arval($input, 'url');
				$product->shipping_price = Util::arval($input, 'shipping');
				$product->note = Util::arval($input, 'notes');
				// Try to write to the database
				if (!$product->save()) {
					// Get DB error messages
					$messages = $product->getMessages();
					foreach ($messages as $message) {
						// Add all errors to the API response object
						$this->api_response->add_error($message->getMessage());
					}
					Util::json_response($this->api_response);
				} else {
					// At this point, the product was saved successfully, save the inventory record as well
					$inventory->product_id = $product->id;
					$inventory->sku = $sku;
					$inventory->quantity = Util::arval($input, 'qty', 0);
					$inventory->color = Util::arval($input, 'color');
					$inventory->size = Util::arval($input, 'size');
					$inventory->cost_cents = Util::arval($input, 'cost');
					$inventory->price_cents = $price;
					// Try to write to the database
					if (!$inventory->save()) {
						// Get DB error messages
						$messages = $product->getMessages();
						foreach ($messages as $message) {
							// Add all errors to the API response object
							$this->api_response->add_error($message->getMessage());
						}
						Util::json_response($this->api_response);
					} else {
						// Redirect to a product list since the creation was successful
						$this->api_response->message = "Product $status successfully";
						Util::json_response($this->api_response, null, '/product/list');
					}
				}
			}
		}
	}
}
