<?php
/**
 * Different product types
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class ProductType {
	public const CLOTHING = 'clothing';
	public const FOOD     = 'food';
	public const AUTO     = 'automotive';

	public const ALL = [
		self::CLOTHING,
		self::FOOD,
		self::AUTO,
	];
}
