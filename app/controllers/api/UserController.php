<?php
/**
 * API controller for user CRUD
 *
 * @author  Paul Brighton
 * @package controllers/api
 */

namespace API\Controllers;

use API\Controllers\Base\Controller;
use Entities\Util;
use Services\AuthService;

class UserController extends Controller {

	/**
	 * Tries to log the user in
	 *
	 */
	public function loginAction(): void {
		// Make sure the request is done via POST and AJAX
		if ($this->request->isPost() and $this->request->isAjax()) {
			// Get the JSON input
			if ($input = $this->request->getJsonRawBody(true)) {
				if (!$email = Util::arval($input, 'username')) {
					Util::json_error('Missing email');
				} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					Util::json_error('Invalid email format');
				} elseif (!$password = Util::arval($input, 'password')) {
					Util::json_error('Missing password');
				} else {
					// Authenticate using our auth service
					$response = AuthService::get_shared()->login($email, $password);
					// Redirect to dashboard if the login was successful
					if ($response->success === true) {
						Util::json_response($response, null, '/user/dashboard');
					} else {
						Util::json_response($response);
					}
				}
			} else {
				Util::json_error('Invalid input format');
			}
		} else {
			// Deliberately give an ambiguous error message to avoid disclosing our accepted types
			Util::json_error('Invalid request format');
		}
	}
}
