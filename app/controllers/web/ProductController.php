<?php
/**
 * Products controller
 * ******************************************************
 *
 * @author  Paul Brighton
 * @package controllers/web
 */

namespace Web\Controllers;

use Enums\ProductColor;
use Enums\ProductSize;
use Enums\ProductType;
use Models\Inventory;
use Models\User;
use Phalcon\Http\Response;
use Phalcon\Http\ResponseInterface;
use Services\AuthService;
use Web\Controllers\Base\Controller;

class ProductController extends Controller {

	/**
	 * Default index page
	 * Redirects to the product list action
	 *
	 */
	public function indexAction() {
		return $this->response->redirect('/product/list');
	}

	/**
	 * Lists all the user products
	 *
	 */
	public function listAction() {
		if ($user = AuthService::get_loggedin_user()) {
			$inventories = [];
			// First, get the user object based on the user id stored in session
			if ($user = User::find_by_id($user['id'])) {
				// Then get all inventories associated with this user
				$inventories = $user->get_inventories();
			}
			// See if we have an error session key
			$error = '';
			if ($this->session->has('error')) {
				$error = $this->session->get('error');
				// Free the error
				$this->session->remove('error');
			}
			// Pass the inventories to the view
			$this->view->setVars([
				'inventories' => $inventories,
				'error'       => $error,
			]);
		} else {
			return $this->response->redirect('/user/login');
		}
	}

	/**
	 * Renders a page to add new product
	 *
	 */
	public function addAction() {
		// Allow only the loggedin users
		if (!$user = AuthService::get_loggedin_user()) {
			return $this->response->redirect('/user/login');
		}
		// Generate a list of product types, colors, and sizes
		$this->view->setVars([
			'types'  => ProductType::ALL,
			'colors' => ProductColor::ALL,
			'sizes'  => ProductSize::ALL,
		]);
	}

	/**
	 * Renders a page to edit an existing product
	 *
	 * @param int|null $id
	 * @return Response|ResponseInterface
	 */
	public function editAction($id = null) {
		// Allow only the loggedin users
		if (!$user = AuthService::get_loggedin_user()) {
			return $this->response->redirect('/user/login');
		}
		// Make sure the id was given
		if ($id) {
			// Now make sure the id is valid
			if (!$inventory = Inventory::find_by_id($id)) {
				$this->session->set('error', 'Product/Inventory ID is invalid');
				return $this->response->redirect('/product/list');
			}
		} else {
			$this->session->set('error', 'No ID given to edit a product');
			return $this->response->redirect('/product/list');
		}
		// Generate a list of product types, colors, and sizes
		$this->view->setVars([
			'inventory' => $inventory,
			'types'     => ProductType::ALL,
			'colors'    => ProductColor::ALL,
			'sizes'     => ProductSize::ALL,
		]);
	}

}