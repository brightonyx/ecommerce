<?php
/**
 * Base controller that extends the native phalcon controller
 *
 * @author  Paul Brighton
 * @package controllers/web
 */

namespace Web\Controllers\Base;

use Phalcon\Di;

abstract class Controller extends \Phalcon\Mvc\Controller {

	/**
	 * Initializes the controllers
	 * Base class defines some global variables that can be used in any extending controller
	 *
	 */
	public function initialize() {
		/** @var Di $di */
		$di = Di::getDefault();
	}
}
