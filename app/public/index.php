<?php
/**
 * Main loader file that initializes services, models, namespaces, etc
 *
 * @author  Paul Brighton
 * @package bootstrap
 */

use Phalcon\Debug;
use Phalcon\Http\Request;
use Phalcon\DI\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\Application;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Session\Adapter\Files as Session;

// Also define some global variables
define('APPLICATION_ROOT', dirname(__FILE__, 3));
define('APPLICATION_PATH', APPLICATION_ROOT . '/app');

// Load vendor libraries from composer
require APPLICATION_ROOT . '/vendor/autoload.php';

// Enable the native debug component that prints pretty messages
$debug = new Debug();
$debug->listen();

// Register our namespaces
$loader = new Loader();
$loader->registerNamespaces(
	[
		// Shared classes such as services and models
		'Services\Base'        => APPLICATION_PATH . '/services/base',
		'Services'             => APPLICATION_PATH . '/services',
		'Entities'             => APPLICATION_PATH . '/entities',
		'Models\Base'          => APPLICATION_PATH . '/models/base',
		'Models'               => APPLICATION_PATH . '/models',
		'Enums'                => APPLICATION_PATH . '/enums',

		// Custom controllers for web and api
		'Web\Controllers\Base' => APPLICATION_PATH . '/controllers/web/base',
		'Web\Controllers'      => APPLICATION_PATH . '/controllers/web',
		'API\Controllers\Base' => APPLICATION_PATH . '/controllers/api/base',
		'API\Controllers'      => APPLICATION_PATH . '/controllers/api',
	]
)->register();

// Instantiate a dependency injector.
$di = new FactoryDefault();

// Setup the view component
$di->setShared(
	'view',
	function () use ($di) {
		$view = new View();
		$view->setViewsDir(APPLICATION_PATH . '/views');
		// Define basic information about the views and where the cache folder is located
		$view->registerEngines(
			[
				'.volt' => function ($view) use ($di) {
					$volt = new Volt($view, $di);
					$volt->setOptions(
						[
							'compiledPath'      => APPLICATION_ROOT . '/cache/volt/',
							'compiledSeparator' => '_',
							'compileAlways'     => true
						]
					);
					$compiler = $volt->getCompiler();
					// Add a custom filter to Volt: format money
					$compiler->addFilter('money', function($main_args, $expression_args) {
						return "number_format($main_args, 2)";
					});
					return $volt;
				},
			]
		);
		return $view;
	}
);

// Get the reference to events manager to tap into specific execution steps
$events_manager = new EventsManager();

// Create a database service
$di->setShared('db', function () {
	try {
		return new DbAdapter([
			'host'     => 'localhost',
			'username' => 'root',
			'password' => 'root',
			'dbname'   => 'ecommerce',
		]);
	} catch (\Exception $e) {
		die('Failed to connect to the database (localhost): ' . $e->getMessage());
	}
});

// Using the EventsManager tap into the execution process and listen on exception and failures
$di->setShared(
	'dispatcher',
	function () use ($events_manager) {
		// Listen before exception. Usually 404 errors
		$events_manager->attach(
			'dispatch:beforeException',
			function ($event, $dispatcher, $exception) {
				/** @var Events\Event $event */
				/** @var Dispatcher $dispatcher */
				/** @var Exception $exception */
				// Handle 404 exceptions
				if ($exception instanceof Dispatcher\Exception) {
					$dispatcher->forward(['controller' => 'index', 'action' => 'not_found']);
					return false;
				}
				// Alternative way, controller or action doesn't exist
				if ($event->getType() === 'beforeException') {
					switch ($exception->getCode()) {
						case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
						case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
							$dispatcher->forward(['controller' => 'index', 'action' => 'not_found']);
							return false;
					}
				}
				return true;
			}
		);

		$dispatcher = new Dispatcher();
		$dispatcher->setEventsManager($events_manager);
		return $dispatcher;
	}
);

// Add routing capabilities
$di->set(
	'router',
	function () {
		$router = new Router();

		// Remove trailing slashes automatically
		$router->removeExtraSlashes(true);

		// Define a default route
		$router->addGet('/', [
			'controller' => 'index',
			'action'     => 'index'
		])->setName('default');

		// Add the API layer
		$router->add('/api/:controller/:action/:params', [
			'namespace'  => 'API\Controllers',
			'controller' => 1,
			'action'     => 2,
			'params'     => 3
		])->setName('api');

		// 404 errors
		$router->notFound([
			'controller' => 'index',
			'action'     => 'not_found',
		]);

		// Set the default namespace - important!
		$router->setDefaultNamespace('Web\Controllers');

		return $router;
	}
);

// Start the session the first time when some component request the session service
$di->setShared(
	'session',
	function () {
		$session = new Session();
		$session->start();
		return $session;
	}
);

// Handle the request
$application = new Application($di);

// Attach events at various stages of application execution
$application->setEventsManager($events_manager);

try {
	// Handle the request
	$request  = new Request();
	$response = $application->handle($request->getURI());
	// Run the app
	$response->send();
} catch (Exception $e) {
	echo $e->getMessage();
}

