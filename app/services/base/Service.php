<?php
/**
 * Base Service
 *
 * @author  Paul Brighton
 * @package services/base
 */

namespace Services\Base;

use Phalcon\Di;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Security;
use Entities\ResponsePayload;
use Phalcon\Session\Adapter\Files as Session;

abstract class Service {

	/** @var \Phalcon\DiInterface|Di $di The phalcon dependency injector. */
	protected $di;

	/** @var Request $request */
	protected $request;

	/** @var Response $response Instance of an actual Phalcon response object */
	protected $response;

	/** @var ResponsePayload $api_response Instance of our custom ResponsePayload object */
	protected $api_response;

	/** @var Filter $filter */
	protected $filter;

	/** @var Security $security */
	protected $security;

	/** @var Session $session */
	protected $session;

	/**
	 * Wrapper to get shared instance of this service. We will use shared to use singleton objects
	 * Because DI is not fully initialized on construct, we run the init logic here
	 *
	 * @return static
	 */
	public static function get_shared() {
		/** @var Di $di */
		$di = Di::getDefault();
		/** @var static $service */
		$service = $di->getShared(static::class);
		// Save a reference to the phalcon dependency injector to be accessible from any extending service
		$service->di = $di;
		// Similarly, get the DB and other references
		$service->request      = $di->has('request') ? $di->getShared('request') : null;
		$service->response     = $di->has('response') ? $di->getShared('response') : null;
		$service->security     = $di->has('security') ? $di->getShared('security') : null;
		$service->session      = $di->has('session') ? $di->getShared('session') : null;
		$service->api_response = new ResponsePayload();

		// Return the awesomeness
		return $service;
	}
}
