# eCommerce Boilerplate

This is a simple ecommerce starter pack that has basic ORM operations. The project was built using PhalconPHP and MySQL on the backend, and simple jQuery and [sb admin 2](https://startbootstrap.com/themes/sb-admin-2/) template on the frontend. No JS/SCSS compilers nor minifications are used.  

**To build the project locally, you need to perform the following steps:**

- [download](https://phalcon.io/en-us/download/linux) phalconPHP 3.4 binary or compile it manually for your OS  
- configure a web server document root to point to `ecommerce/app/public/index.php`
- create writable folders (*chmod 777*) that don't exist in the repo: `ecommerce/cache/volt` and `ecommerce/logs`
- run `composer install`. There is no npm in this project, only CDN and few local includes
- finally, create and seed your database. Initial files are located [here](https://bitbucket.org/brightonyx/ecommerce/src/master/database/seed.sql.7z)
