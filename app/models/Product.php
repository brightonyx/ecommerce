<?php
/**
 * Products model
 *
 * @author  Paul Brighton
 * @package models
 */

namespace Models;

use Phalcon\Mvc\Model\Relation;
use Enums\ProductType;
use Models\Base\Model;

/**
 * @property User $admin
 * @property Inventory[] $inventories
 * @property Order[] $orders
 */
class Product extends Model {

	/** @var string $name */
	public $name;

	/** @var ProductType|string $type */
	public $type;

	/** @var string $description */
	public $description;

	/** @var string $style */
	public $style;

	/** @var string $brand */
	public $brand;

	/** @var string $url */
	public $url;

	/** @var float $shipping_price */
	public $shipping_price;

	/** @var string $note */
	public $note;

	/** @var int|null $admin_id Owner/admin of this product */
	public $admin_id;

	/**
	 * Applies logic that needs to happen on construct - in this case defines the relationships
	 *
	 */
	public function initialize(): void {
		$this->hasMany(
			'id',
			Order::class,
			'product_id',
			[
				'alias' => 'orders',
				'foreignKey' => [
					'action' => Relation::ACTION_CASCADE,
				]
			]
		);

		$this->hasMany(
			'id',
			Inventory::class,
			'product_id',
			[
				'alias' => 'inventories',
				'foreignKey' => [
					'action' => Relation::ACTION_CASCADE,
				]
			]
		);


		$this->belongsTo(
			'admin_id',
			User::class,
			'id',
			['alias' => 'admin']
		);

		// Since the table name is plural, change it manually
		$this->setSource('products');
	}

}
