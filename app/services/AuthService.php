<?php
/**
 * Authentication service is used for all user-related auth calls: login, JWT generation, etc
 *
 * @author  Paul Brighton
 * @package services
 */

namespace Services;

use Entities\ResponsePayload;
use Models\User;
use Services\Base\Service;

class AuthService extends Service {

	/**
	 * Checks if the user is logged in by checking the session
	 *
	 * @return array|null
	 */
	public static function get_loggedin_user(): ?array {
		$self = self::get_shared();
		if ($self->session->has('user')) {
			return $self->session->get('user');
		}

		// Nothing found
		return null;
	}

	/**
	 * Authenticates a user given the correct credentials
	 *
	 * @param string $email Username or email
	 * @param string $password Password
	 * @return ResponsePayload
	 */
	public function login(string $email, string $password = null): ResponsePayload {
		// First make sure the username/email belongs to a real active user
		$user = User::find_first([
			'conditions' => 'email=:email: AND is_enabled=true',
			'bind'       => ['email' => $email],
		]);
		if ($user) {
			// Is the password valid or we want to force a login
			if ($this->security->checkHash($password, $user->password_hash)) {
				try {
					// For JWT, this is where we would generate bearer token
					// In this example however, we will simply store data in a session
					$this->session->set('user', [
						'id'         => $user->id,
						'email'      => $user->email,
						'name'       => $user->name,
						'shop_name'  => $user->shop_name,
						'superadmin' => $user->superadmin,
					]);
				} catch (\Exception $e) {
					$this->api_response->add_error($e->getMessage());
				}
			} else {
				$this->api_response->add_error('Email or password is incorrect');
			}
		} else {
			$this->api_response->add_error('No such user exists in our database');
		}

		return $this->api_response;
	}
}
