<?php
/**
 * Different product colors
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class ProductColor {
	public const GREY         = 'Grey';
	public const LIGHT_GREY   = 'Light Grey';
	public const BLACK        = 'Black';
	public const PINK         = 'Pink';
	public const METALLIC_RED = 'Metallic Red';
	public const CHERRY_RED   = 'Cherry Red';
	public const BLUE         = 'Blue';
	public const STEEL_BLUE   = 'Steel Blue';
	public const DARK_BLUE    = 'Dark Blue';
	public const GREEN        = 'Green';
	public const CHECKERED    = 'Checkered';
	public const PURPLE       = 'Purple';
	public const BEIGE        = 'Beige';
	public const ORANGE       = 'Orange';

	public const ALL = [
		self::GREY,
		self::LIGHT_GREY,
		self::BLACK,
		self::PINK,
		self::METALLIC_RED,
		self::CHERRY_RED,
		self::BLUE,
		self::STEEL_BLUE,
		self::DARK_BLUE,
		self::GREEN,
		self::CHECKERED,
		self::PURPLE,
		self::BEIGE,
		self::ORANGE,
	];
}
