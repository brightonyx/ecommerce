<?php
/**
 * Inventory model
 *
 * @author  Paul Brighton
 * @package models
 */

namespace Models;

use Models\Base\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * @property Product $product
 * @property Order[] $orders
 */
class Inventory extends Model {

	/** @var int $product_id */
	public $product_id;

	/** @var string $sku */
	public $sku;

	/** @var int $quantity */
	public $quantity = 0;

	/** @var string $color */
	public $color;

	/** @var string $size */
	public $size;

	/** @var float $weight */
	public $weight;

	/** @var float $length */
	public $length;

	/** @var float $width */
	public $width;

	/** @var float $height */
	public $height;

	/** @var int $price_cents */
	public $price_cents;

	/** @var int $sale_price_cents */
	public $sale_price_cents;

	/** @var int $cost_cents */
	public $cost_cents;

	/** @var string $note */
	public $note;

	/**
	 * Applies logic that needs to happen on construct - in this case defines the relationships
	 */
	public function initialize(): void {
		$this->belongsTo(
			'product_id',
			Product::class,
			'id',
			[
				'alias' => 'product',
				'foreignKey' => [
					'action' => Relation::ACTION_CASCADE,
				]
			]
		);
		$this->hasMany(
			'id',
			Order::class,
			'inventory_id',
			[
				'alias' => 'orders',
				'foreignKey' => [
					'action' => Relation::ACTION_CASCADE,
				]
			]
		);
	}
}
