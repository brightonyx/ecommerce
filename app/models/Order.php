<?php
/**
 * Orders model
 *
 * @author  Paul Brighton
 * @package models
 */

namespace Models;

use Enums\CountryCode;
use Enums\OrderStatus;
use Models\Base\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * @property Product $product
 * @property Inventory $inventory
 */
class Order extends Model {

	/** @var int $product_id */
	public $product_id;

	/** @var int $inventory_id */
	public $inventory_id;

	/** @var string $street_address */
	public $street_address;

	/** @var string $apartment */
	public $apartment;

	/** @var string $city */
	public $city;

	/** @var string $state_province */
	public $state_province;

	/** @var string $postal_code */
	public $postal_code;

	/** @var CountryCode|string $country_code */
	public $country_code;

	/** @var string|int $phone_number */
	public $phone_number;

	/** @var string $email */
	public $email;

	/** @var string $name */
	public $name;

	/** @var OrderStatus|string $order_status */
	public $order_status;

	/** @var string $payment_ref */
	public $payment_ref;

	/** @var string|int $transaction_id */
	public $transaction_id;

	/** @var int $payment_amount_cents */
	public $payment_amount_cents;

	/** @var int $ship_charged_cents */
	public $ship_charged_cents;

	/** @var int $ship_cost_cents */
	public $ship_cost_cents;

	/** @var int $subtotal_cents */
	public $subtotal_cents;

	/** @var int $total_cents */
	public $total_cents;

	/** @var int $tax_total_cents */
	public $tax_total_cents;

	/** @var string $shipper_name */
	public $shipper_name;

	/** @var string|int $payment_date */
	public $payment_date;

	/** @var string|int $shipped_date */
	public $shipped_date;

	/** @var string|int $tracking_number */
	public $tracking_number;

	/**
	 * Applies logic that needs to happen on construct - in this case defines the relationships
	 */
	public function initialize(): void {
		$this->belongsTo(
			'product_id',
			Product::class,
			'id',
			[
				'alias' => 'product',
				'foreignKey' => [
					'action' => Relation::ACTION_CASCADE,
				]
			]
		);
		$this->belongsTo(
			'inventory_id',
			Inventory::class,
			'id',
			[
				'alias' => 'inventory',
				'foreignKey' => [
					'action' => Relation::ACTION_CASCADE,
				]
			]
		);

		// Since the table name is plural, change it manually
		$this->setSource('orders');
	}

}
