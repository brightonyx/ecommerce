<?php
/**
 * Response message entity. Contains details about response variables passed back from our API.
 * This is a low level object that only combines success and error messages as well as any additional data.
 *
 * @author  Paul Brighton
 * @package entities
 */

namespace Entities;

use Enums\Date;
use Phalcon\Text;

class ResponsePayload {

	/** @var string $id Random ID string to distinguish different response messages */
	public $id;

	/** @var bool $success True by default, changes to false automatically if any errors are detected in the $errors array */
	public $success;

	/** @var string[] $errors List of errors (if any) that occurred during the operation */
	public $errors;

	/** @var string|null $message Final message to display to the client */
	public $message;

	/** @var array $data Send additional information that the API generated */
	public $data;

	/** @var string $created_at */
	public $created_at;

	/**
	 * Class constructor
	 *
	 */
	public function __construct() {
		// Generate a random hash
		$this->id     = Text::random(Text::RANDOM_ALNUM, 16);
		$this->errors = $this->data = [];
		// Treat this is a successful response by default
		$this->success    = true;
		$this->created_at = date(Date::SYSTEM_LOG);
	}

	/**
	 * Adds a new severe error to the stack
	 *
	 * @param string $message Error text
	 */
	public function add_error($message): void {
		// Set the success status to false
		$this->success = false;

		// Save the original message in the errors array
		$this->errors[] = $message;
	}
}
