<?php
/**
 * All of the accepted credit card types/brands
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class CreditCard {
	public const DISCOVER = 'Discover';
	public const VISA     = 'Visa';
	public const MC       = 'Mastercard';
	public const AMEX     = 'Amex';

	/** @var array List all the cards */
	public const ALL = [
		self::DISCOVER,
		self::VISA,
		self::MC,
		self::AMEX
	];
}
