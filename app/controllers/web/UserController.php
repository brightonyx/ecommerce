<?php
/**
 * Users controller to manage user creation, updates and logins
 * ******************************************************
 *
 * @author  Paul Brighton
 * @package controllers/web
 */

namespace Web\Controllers;

use Models\Order;
use Models\User;
use Entities\Util;
use Services\AuthService;
use Web\Controllers\Base\Controller;

class UserController extends Controller {

	/**
	 * Default index page
	 * Redirects to the user list action
	 *
	 */
	public function indexAction() {
		return $this->response->redirect('/user/list');
	}

	/**
	 * Renders a basic report
	 *
	 */
	public function dashboardAction() {
		if ($user = AuthService::get_loggedin_user()) {
			$total  = 0;
			$sales  = [];
			$orders = [];
			// First, get the user object based on the user id stored in session
			if ($user = User::find_by_id($user['id'])) {
				// Then iterate through all the product associated with this user
				foreach ($user->managed_products as $managed_product) {
					// Get all the orders for this product
					if ($product_orders = $managed_product->orders and count($product_orders)) {
						// Increment the total by looking at all the orders
						foreach ($product_orders as $order) {
							/** @var Order $order */
							if ($order->total_cents) {
								$total += $order->total_cents;
								// Also store the amount to compute the average later
								$sales[] = $order->total_cents;
							}
							// Add them to a parent list
							$orders[] = $order;
						}
					}
				}

				// Pass the collected information to the view
				$this->view->setVars([
					'orders'  => $orders,
					'total'   => $total / 100,
					'average' => (array_sum($sales)/count($sales)) / 100,
				]);
			}
		} else {
			return $this->response->redirect('/user/login');
		}
	}

	/**
	 * Lists all the users
	 *
	 */
	public function listAction() {
		if (AuthService::get_loggedin_user()) {
			$this->view->setVar('users', Util::printr(User::find(), true));
		} else {
			return $this->response->redirect('/user/login');
		}
	}

	/**
	 * Shows the profile of the logged in user
	 *
	 */
	public function profileAction() {
		$user = AuthService::get_loggedin_user();
		$this->view->setVar('user', Util::printr(User::find_by_id($user['id']), true));
	}

	/**
	 * Renders user login page
	 *
	 */
	public function loginAction(){
		// Redirect to dashboard if user is logged in
		if (AuthService::get_loggedin_user()) {
			return $this->response->redirect('/user/dashboard');
		}
		// Render the login volt here
	}

	/**
	 * Method to log user out by flushing the session
	 *
	 */
	public function logoutAction(){
		$this->session->regenerateId();
		$this->session->destroy();
		return $this->response->redirect('/user/login');
	}
}