<?php
/**
 * Base model - acts as a glue to constructing any objects that have a reference to a database table
 *
 * @author  Paul Brighton
 * @package models/base
 */

namespace Models\Base;

use Enums\Date;
use Phalcon\Mvc\Model as PhalconModel;

abstract class Model extends PhalconModel {
	// The following three fields are common to every model

	/** @var int $id A long int that uniquely identifies each model instance within the database */
	public $id;

	/** @var string|null $updated_at The time at which the model instance was last updated */
	public $updated_at;

	/** @var string $created_at The time at which the model instance was created */
	public $created_at;


	/**
	 * Wrapper for the find_first() method which fetches only one model instance by a specific id
	 *
	 * @param int $id
	 * @return static|PhalconModel|null
	 */
	public static function find_by_id($id) {
		return self::find_first((int)$id);
	}

	/**
	 * Wrapper for the find_first() method which fetches the first model in a list with parameters ranging from an id to a a criteria array
	 *
	 * @param array|string|int|null $parameters
	 * @return static|PhalconModel|null
	 */
	public static function find_first($parameters = null) {
		return parent::findFirst($parameters);
	}

	/**
	 * System method that inserts a created_at timestamp before a new record is created
	 */
	public function beforeCreate() {
		$this->created_at = date(Date::SYSTEM_LOG);
	}

	/**
	 * System method that inserts a updated_at timestamp before a record is updated
	 */
	public function beforeUpdate() {
		$this->updated_at = date(Date::SYSTEM_LOG);
	}
}