{#
 *
 * Content for the list of products
 * ******************************************************
 *
 * @author   Paul Brighton
 * @package  views
#}

{% extends "templates/main.volt" %}

{% block title %}List of products{% endblock %}

{% block head %}

{% endblock %}

{% block content %}


					<div class="container-fluid">
						<!-- Page Heading -->
						<h1 class="h3 mb-4 text-gray-800">Add a new product</h1>
						<div class="row">
							<div class="col-lg-4">
								<!-- Default Card Example -->
								<div class="card mb-4">
									<div class="card-header">
										Details
									</div>
									<div class="card-body">
										<form id="add-product-form">
											<div class="form-group">
												<input type="text" class="form-control" id="sku" placeholder="SKU" required>
											</div>
											<div class="form-group">
												<input type="text" class="form-control" id="name" placeholder="Name" required>
											</div>
											<div class="form-group">
												<select class="form-control" id="type">
													<option value="">Type</option>
													{% for type in types %}

													<option value="{{ type }}">{{ type|capitalize }}</option>
													{% endfor %}
												</select>
											</div>
											<div class="form-group">
												<textarea class="form-control" id="description" rows="3" placeholder="Description"></textarea>
											</div>
											<div class="form-group">
												<input type="text" class="form-control" id="style" placeholder="Style">
											</div>
											<div class="form-group">
												<select class="form-control" id="color">
													<option value="">Color</option>
													{% for color in colors %}

													<option>{{ color }}</option>
													{% endfor %}
												</select>
											</div>
											<div class="form-group">
												<select class="form-control" id="size">
													<option value="">Size</option>
													{% for size in sizes %}

													<option>{{ size }}</option>
													{% endfor %}
												</select>
											</div>
											<div class="form-group">
												<input type="number" class="form-control" id="qty" placeholder="Quantity">
											</div>
											<div class="form-group">
												<input type="text" class="form-control" id="brand" placeholder="Brand">
											</div>
											<div class="form-group">
												<input type="text" class="form-control" id="url" placeholder="URL">
											</div>
											<div class="form-group">
												<input type="number" class="form-control" id="price" placeholder="Price (in cents)" required>
											</div>
											<div class="form-group">
												<input type="number" class="form-control" id="cost" placeholder="Cost (in cents)" required>
											</div>
											<div class="form-group">
												<input type="number" class="form-control" id="shipping" placeholder="Shipping Cost (in cents)">
											</div>
											<div class="form-group">
												<textarea class="form-control" id="note" rows="2" placeholder="Notes"></textarea>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary">
													<span class="text">Create New Product</span>
												</button>
												<span class="ml-3 alert alert-success d-none" id="status"></span>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

{% endblock %}


{% block js %}

		<script type="application/javascript">
			$(document).ready(function () {
				$('#add-product-form').on('submit', function (e) {
					e.preventDefault();
					$.ajax({
						url: '/api/product/add',
						method: 'POST',
						contentType: 'application/json',
						data: JSON.stringify({
							sku: $('#sku').val(),
							name: $('#name').val(),
							type: $('#type').val(),
							description: $('#description').val(),
							qty: $('#qty').val(),
							style: $('#style').val(),
							color: $('#color').val(),
							size: $('#size').val(),
							brand: $('#brand').val(),
							url: $('#url').val(),
							price: $('#price').val(),
							cost: $('#cost').val(),
							shipping: $('#shipping').val(),
							notes: $('#notes').val(),
						}),
						fail: function () {
							renderError(null, 'Error occurred processing your request');
						},
						error: function (xhr, textStatus, errorThrown) {
							renderError(xhr.responseJSON, errorThrown);
						},
						success: function (response) {
							if (response) {
								if (response.success === true) {
									// On success, show a message and redirect to product list after 3 seconds
									$('#status').text(response.payload.message).removeClass('d-none');
									setTimeout(function () {
										window.location.href = response.redirect;
									}, 3000);
								} else {
									renderError(response);
								}
							} else {
								renderError(null, 'No data received from the server');
							}
						}
					});
				});
			});
		</script>

{% endblock %}