<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="Paul Brighton">
	<title>Login</title>
	<!-- Custom fonts for this template-->
	<link href="/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="/assets/css/sb-admin-2.min.css" rel="stylesheet">
</head>
<body class="bg-gradient-primary">
	<div class="container">
		<!-- Outer Row -->
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-12 col-md-9">
				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row">
							<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
							<div class="col-lg-6">
								<div class="p-5">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
									</div>
									<form class="user" id="login-form">
										<div class="form-group">
											<input type="email" class="form-control form-control-user" id="username" required placeholder="Email Address">
										</div>
										<div class="form-group">
											<input type="password" class="form-control form-control-user" id="password" required placeholder="Password">
										</div>
										<div class="form-group">
											<div class="custom-control custom-checkbox small">
												<input type="checkbox" class="custom-control-input" id="remember-me">
												<label class="custom-control-label" for="remember-me">Remember Me</label>
											</div>
										</div>
										<button class="btn btn-primary btn-user btn-block" id="form-submit">Login</button>
									</form>
									<hr>
									<div class="text-center">
										<a class="small" href="/user/forgot-password">Forgot Password?</a>
									</div>
									<div class="text-center">
										<a class="small" href="/user/register">Create an Account!</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Modal-->
	<div class="modal fade" id="status-modal" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-title"></h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="btn btn-info btn-circle btn-sm mr-3"> <i class="fas fa-info"></i> </div>
					<span id="modal-body"></span>
				</div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<!-- Bootstrap core JavaScript-->
	<script src="/assets/vendor/jquery/jquery.min.js"></script>
	<script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Core plugin JavaScript-->
	<script src="/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="/assets/js/sb-admin-2.js"></script>
	<script type="application/javascript">
		$(document).ready(function () {
			$('#login-form').on('submit', function (e) {
				e.preventDefault();
				let username = $('#username').val();
				let password = $('#password').val();
				// Make sure both email and password are not empty
				if (username && password) {
					$.ajax({
						url: '/api/user/login',
						method: 'POST',
						contentType: 'application/json',
						data: JSON.stringify({username:username, password:password}),
						fail: function () {
							renderError(null, 'Error occurred processing your request');
						},
						error: function (xhr, textStatus, errorThrown) {
							renderError(xhr.responseJSON, errorThrown);
						},
						success: function (response) {
							if (response) {
								if (response.success === true) {
									// On success, redirect to dashboard
									window.location.href = response.redirect;
								} else {
									renderError(response);
								}
							} else {
								renderError(null, 'No data received from the server');
							}
						}
					});
				} else {
					$('#username').focus();
				}
			});
		});
	</script>
</body>
</html>