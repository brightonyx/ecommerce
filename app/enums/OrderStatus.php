<?php
/**
 * Order status type
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class OrderStatus {
	public const FULFILLED = 'Fulfulled';
	public const SHIPPED   = 'Shipped';
	public const OPEN      = 'Open';
	public const PAID      = 'Paid';
}
