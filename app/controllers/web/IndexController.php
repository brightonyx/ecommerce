<?php
/**
 * Index controller - aka the landing page
 *
 * @author  Paul Brighton
 * @package controllers/web
 */

namespace Web\Controllers;

use Entities\Util;
use Models\Order;
use Models\Product;
use Models\User;
use Services\AuthService;
use Web\Controllers\Base\Controller;

class IndexController extends Controller {

	/**
	 * Renders the main landing page
	 *
	 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
	public function indexAction() {
		// If the user is logged in, take them to the dashboard
		if (!$user = AuthService::get_loggedin_user()) {
			return $this->response->redirect('/user/login');
		}
		// Otherwise to a dashboard page
		return $this->response->redirect('/user/dashboard');
	}


	/**
	 * Renders the 404 not found page
	 *
	 */
	public function not_foundAction(): void {
		// Return a 404 status code to the client to indicate no page is available at this URL
		$this->response->setStatusCode(404);

		// Renders not_found.volt view page here. 
	}
}
