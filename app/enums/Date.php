<?php
/**
 * Different date formats
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class Date {

	public const ISO           = 'c';                  // 2020-07-04T14:00:00-04:00
	public const SYSTEM_DATE   = 'Y-m-d';              // 2020-07-04
	public const SYSTEM_TIME   = 'H:i:s';              // 18:05:00
	public const SYSTEM_LOG    = 'Y-m-d H:i:s';        // 2020-06-20 18:05:00
}