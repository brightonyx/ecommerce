{#
 *
 * Content for the reports
 * ******************************************************
 *
 * @author   Paul Brighton
 * @package  views
#}

{% extends "templates/main.volt" %}

{% block title %}Orders report{% endblock %}

{% block head %}

		<link href="/assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

{% endblock %}

{% block content %}


					<div class="container-fluid">
						<!-- Page Heading -->
						<h1 class="h3 mb-4 text-gray-800">Reports</h1>
						<div class="row">
							<div class="col-xl-3 col-md-6 mb-4">
								<div class="card border-left-primary shadow h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Sales</div>
												<div class="h5 mb-0 font-weight-bold text-gray-800">${{ total|money }}</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-md-6 mb-4">
								<div class="card border-left-success shadow h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Average Price</div>
												<div class="h5 mb-0 font-weight-bold text-gray-800">${{ average|money }}</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="card mb-4">
							<div class="card-header">
								Orders
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered" id="orders" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Customer</th>
												<th>Email</th>
												<th>Product</th>
												<th>Color</th>
												<th>Size</th>
												<th>Status</th>
												<th>Total</th>
												<th>Transaction ID</th>
												<th>Shipper</th>
												<th>Tracking #</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Customer</th>
												<th>Email</th>
												<th>Product</th>
												<th>Color</th>
												<th>Size</th>
												<th>Status</th>
												<th>Total</th>
												<th>Transaction ID</th>
												<th>Shipper</th>
												<th>Tracking #</th>
											</tr>
										</tfoot>
										<tbody>
											{% for order in orders %}

												<tr>
													<td>{{ order.name }}</td>
													<td>{{ order.email }}</td>
													<td>{{ order.product.name }}</td>
													<td>{{ order.inventory.color }}</td>
													<td>{{ order.inventory.size }}</td>
													<td>{{ order.order_status }}</td>
													<td>${{ (order.total_cents / 100)|money }}</td>
													<td>{{ order.transaction_id }}</td>
													<td>{{ order.shipper_name }}</td>
													<td>{{ order.tracking_number }}</td>
												</tr>
											{% endfor %}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

{% endblock %}


{% block js %}

		<script src="/assets/vendor/datatables/jquery.dataTables.min.js"></script>
		<script src="/assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
		<script type="application/javascript">
			$(document).ready(function() {
				$('#orders').DataTable();
			});
		</script>

{% endblock %}