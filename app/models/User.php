<?php
/**
 * Users model
 *
 * @author  Paul Brighton
 * @package models
 */

namespace Models;

use Enums\CreditCard;
use Models\Base\Model;
use Phalcon\Mvc\Model\QueryInterface;
use Phalcon\Mvc\Model\Resultset\Simple;

/**
 * @property Product[] $managed_products
 */
class User extends Model {

	/** @var string $name */
	public $name;

	/** @var string $email */
	public $email;

	/** @var string $password_hash Encrypted password - we're using native phalcon security->hash() function to encrypt using bcrypt "Eksblowfish" algorithm */
	public $password_hash;

	/** @var string $password_plain Passwords in clear text to match the hash */
	public $password_plain;

	/** @var bool $superadmin Identify if this is a super admin - should technically be a role but used here for simplicity */
	public $superadmin;

	/** @var string $shop_name Name of the shop that this user owns. Technically should be in a different table but used here for simplicity */
	public $shop_name;

	/** @var string $shop_domain Domain of the shop that this user owns. Technically should be in a different table but used here for simplicity */
	public $shop_domain;

	/** @var string $remember_token None generated when user forget the password */
	public $remember_token;

	/** @var CreditCard|string $card_brand CC type */
	public $card_brand;

	/** @var int $card_last_four Last four digits of a given credit card */
	public $card_last_four;

	/** @var bool $is_enabled Active flag - defaults to true */
	public $is_enabled = true;

	/** @var string $billing_plan Plan this user is on - also should be in a separate table */
	public $billing_plan;

	/** @var string $trial_starts_at */
	public $trial_starts_at;

	/** @var string $trial_ends_at */
	public $trial_ends_at;

	/**
	 * Applies logic that needs to happen on construct - in this case defines the relationships
	 *
	 */
	public function initialize(): void {
		$this->hasMany(
			'id',
			Product::class,
			'admin_id',
			['alias' => 'managed_products']
		);

		// Since the table name is plural, change it manually
		$this->setSource('users');
	}

	/**
	 * Helper method to return all inventories that have a product that this user manages
	 *
	 * @return QueryInterface|Simple
	 */
	public function get_inventories() {
		return $this->getModelsManager()->executeQuery('SELECT i.* FROM \Models\Inventory i LEFT JOIN \Models\Product p ON p.admin_id = :id: WHERE i.product_id = p.id', ['id' => $this->id]);
	}
}
