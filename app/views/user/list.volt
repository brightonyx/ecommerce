{#
 *
 * Content for the list of users
 * ******************************************************
 *
 * @author   Paul Brighton
 * @package  views
#}

{% extends "templates/main.volt" %}

{% block title %}List of users{% endblock %}

{% block head %}

{% endblock %}

{% block content %}


					<div class="container-fluid">
						<!-- Page Heading -->
						<h1 class="h3 mb-4 text-gray-800">List of all users</h1>
						<div class="row">
							<div class="col-lg-6">
								<!-- Default Card Example -->
								<div class="card mb-4">
									<div class="card-header">
										Output
									</div>
									<div class="card-body">
										{{ users }}
									</div>
								</div>
							</div>
						</div>
					</div>

{% endblock %}


{% block js %}


{% endblock %}