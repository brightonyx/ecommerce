<?php
/**
 * Utility class that includes various helper functions, wrappers and tools to aid the main app development
 *
 * @author  Paul Brighton
 * @package entities
 */

namespace Entities;

use Phalcon\Mvc\Model\Resultset;
use function is_array;
use Models\Base\Model;
use Phalcon\Http\Response;

class Util {

	/**
	 * Prints the contents of an array of an object in a human readable format
	 *
	 * @param object|array|string $entity The entity to print - could be object, array or string
	 * @param bool                $return To return the formatted variable or output directly on the screen
	 * @return string|null
	 */
	public static function printr($entity, $return = false) {
		// If this is an array of objects, apply a friendly print to our classes
		if (is_array($entity)) {
			$results = [];
			foreach ($entity as $key => $item) {
				if ($item instanceof Model or $item instanceof Resultset) {
					$item = $item->toArray();
				}
				$results[$key] = $item;
			}
			$result = '<pre>' . print_r($results, true) . '</pre>';
		} else {
			// Passthrough for existing models
			if ($entity instanceof Model or $entity instanceof Resultset) {
				$entity = $entity->toArray();
			}
			$result = '<pre>' . print_r($entity, true) . '</pre>';
		}
		if ($return) {
			return $result;
		}
		echo $result;
	}

	/**
	 * Gets the value from given array if key is set and is not null/empty string, otherwise returns $default
	 *
	 * @param array      $array
	 * @param string     $key
	 * @param mixed|null $default
	 * @return mixed
	 */
	public static function arval(array $array, $key, $default = null) {
		return (isset($array[$key]) and $array[$key] !== null and $array[$key] !== '') ? $array[$key] : $default;
	}

	/**
	 * Acts as a wrapper to send json error response back to the client
	 *
	 * @param string $message Error message
	 * @param string $redirect Optional URI to redirect to
	 * @param int $status HTTP status code
	 */
	public static function json_error($message, $redirect = null, $status = 400): void {
		$response = new ResponsePayload();
		$response->add_error($message);
		self::json_response($response, $status, $redirect);
	}

	/**
	 * Acts as a wrapper to send json success response back to the client
	 *
	 * @param string $message Success message
	 * @param string $redirect Optional URI to redirect to
	 * @param int    $status  HTTP status code
	 */
	public static function json_success($message, $redirect = null, $status = 200): void {
		$response          = new ResponsePayload();
		$response->message = $message;
		$response->success = true;
		self::json_response($response, $status, $redirect);
	}

	/**
	 * Acts as a wrapper to send json data response back to the client
	 *
	 * @param object|array $data
	 * @param int          $status HTTP status code
	 */
	public static function json_data($data, $status = 200): void {
		$response       = new ResponsePayload();
		$response->data = $data;
		self::json_response($response, $status);
	}

	/**
	 * Sends the ResponsePayload object as JSON back to the client
	 *
	 * @param ResponsePayload $response Constructed response object
	 * @param int|null        $status   HTTP status code
	 * @param string          $redirect Optional URI to redirect to
	 */
	public static function json_response(ResponsePayload $response, $status = null, $redirect = null): void {
		// Handle success
		if ($response->success === true) {
			// We will move the success status out to the root level for easy access, hence we can remove the duplicate here
			unset($response->success);
			self::send_json(['success' => true, 'payload' => $response, 'redirect' => $redirect], $status ?? 200);
		} else {
			unset($error, $response->success);
			self::send_json(['success' => false, 'payload' => $response, 'redirect' => $redirect], $status ?? 400);
		}
	}

	/**
	 * Main logic that sends off json response data and headers including a proper status code
	 *
	 * @param array    $data
	 * @param int|null $status
	 */
	private static function send_json(array $data, $status): void {
		$response = new Response();
		$response->setStatusCode($status, '');
		$response->setContentType('application/json', 'UTF-8');
		$response->setJsonContent($data);
		$response->send();
		exit;
	}
}
