			<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
				<!-- Sidebar - Brand -->
				<a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
					<div class="sidebar-brand-icon rotate-n-15">
						<i class="fas fa-comment-dollar"></i>
					</div>
					<div class="sidebar-brand-text mx-3">eCommerce</div>
				</a>
				<!-- Divider -->
				<hr class="sidebar-divider my-0">
				<!-- Nav Item -->
				<li class="nav-item">
					<a class="nav-link" href="/user/dashboard"> <i class="fas fa-fw fa-tachometer-alt"></i>
						<span>Dashboard</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages"> <i class="fas fa-fw fa-folder"></i>
						<span>Products</span>
					</a>
					<div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
						<div class="bg-white py-2 collapse-inner rounded">
							<a class="collapse-item" href="/product/list">List</a>
							<a class="collapse-item" href="/product/add">Add</a>
						</div>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/user/dashboard"> <i class="fas fa-fw fa-chart-area"></i>
						<span>Reports</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/user/list"> <i class="fas fa-fw fa-user"></i>
						<span>All Users</span>
					</a>
				</li>
				<!-- Divider -->
				<hr class="sidebar-divider d-none d-md-block">
				<!-- Sidebar Toggler (Sidebar) -->
				<div class="text-center d-none d-md-inline">
					<button class="rounded-circle border-0"></button>
				</div>
			</ul>
