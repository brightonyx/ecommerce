{#
 *
 * Content for the list of products
 * ******************************************************
 *
 * @author   Paul Brighton
 * @package  views
#}

{% extends "templates/main.volt" %}

{% block title %}List of products{% endblock %}

{% block head %}

		<link href="/assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

{% endblock %}

{% block content %}


					<div class="container-fluid">
						<!-- Page Heading -->
						<h1 class="h3 mb-4 text-gray-800">List of all inventories with products associated to me</h1>
						<div class="card mb-4">
							<div class="card-header">
								Output
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table class="table table-bordered" id="products" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>SKU</th>
												<th>Name</th>
												<th>Type</th>
												<th>Qty</th>
												<th>Style</th>
												<th>Color</th>
												<th>Size</th>
												<th>Brand</th>
												<th>Price</th>
												<th>Cost</th>
												<th>Shipping</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>SKU</th>
												<th>Name</th>
												<th>Type</th>
												<th>Qty</th>
												<th>Style</th>
												<th>Color</th>
												<th>Size</th>
												<th>Brand</th>
												<th>Price</th>
												<th>Cost</th>
												<th>Shipping</th>
												<th>Actions</th>
											</tr>
										</tfoot>
										<tbody>
											{% for inventory in inventories %}

												<tr>
													<td>{{ inventory.sku }}</td>
													<td>{{ inventory.product.name }}</td>
													<td>{{ inventory.product.type }}</td>
													<td>{{ inventory.quantity }}</td>
													<td>{{ inventory.product.style }}</td>
													<td>{{ inventory.color }}</td>
													<td>{{ inventory.size }}</td>
													<td>{{ inventory.product.brand }}</td>
													<td>${{ (inventory.price_cents / 100)|money }}</td>
													<td>${{ (inventory.cost_cents / 100)|money }}</td>
													<td>${{ (inventory.product.shipping_price / 100)|money }}</td>
													<td class="d-flex justify-content-center" id="actions">
														<a href="/product/edit/{{ inventory.id }}"><i class="fa fa-pen"></i></a>
														<a href="/product/delete/{{ inventory.id }}" class="delete-product" data-name="{{ inventory.product.name }}" data-id="{{ inventory.id }}"><i class="fa fa-trash ml-2"></i></a>
													</td>
												</tr>
											{% endfor %}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

{% endblock %}


{% block js %}

		<script src="/assets/vendor/datatables/jquery.dataTables.min.js"></script>
		<script src="/assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
		<script type="application/javascript">
			$(document).ready(function() {
				$('#products').DataTable();

				// Bind delete button to a confirmation modal
				$('.modal').on('click', '#confirmed', function(){
					$('.modal').modal('hide');
					$.ajax({
						url: '/api/product/delete/' + $(this).data('id'),
						method: 'DELETE',
						fail: function () {
							renderError(null, 'Error occurred processing your request');
						},
						error: function (xhr, textStatus, errorThrown) {
							renderError(xhr.responseJSON, errorThrown);
						},
						success: function (response) {
							if (response) {
								if (response.success === true) {
									// On success, show a message and redirect to product list after 3 seconds
									$('.card-header').html('<span class="alert alert-success">' + response.payload.message + '</span>');
									setTimeout(function () {
										window.location.href = response.redirect;
									}, 3000);
								} else {
									renderError(response);
								}
							} else {
								renderError(null, 'No data received from the server');
							}
						}
					});
				});
				$('#products').on('click', '.delete-product', function(e) {
					e.preventDefault();
					$('#modal-title').text('Are you sure?');
					$('#modal-body').html($(this).data('name') + ' will be permanently deleted along<br>with all inventories and all orders it is linked to');
					$('.modal-footer').html('<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button><button class="btn btn-primary" id="confirmed" data-id="' + $(this).data('id') + '">Confirm</button>');
					$('.modal').modal();
				});
			});
		</script>

{% endblock %}