<?php
/**
 * Different product sizes
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class ProductSize {
	public const XSMALL  = 'XS';
	public const SMALL   = 'S';
	public const MEDIUM  = 'M';
	public const LARGE   = 'L';
	public const XLARGE  = 'XL';
	public const XXLARGE = 'XXL';

	public const ALL = [
		self::XSMALL,
		self::SMALL,
		self::MEDIUM,
		self::LARGE,
		self::XLARGE,
		self::XXLARGE,
	];
}
