<?php
/**
 * Different country codes
 *
 * @author  Paul Brighton
 * @package enums
 */

namespace Enums;

class CountryCode {
	public const US = 'US';
	public const CA = 'CA';
	public const MX = 'MX';
}
