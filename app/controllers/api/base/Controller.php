<?php
/**
 * Base controller that extends the native phalcon controller
 *
 * @author  Paul Brighton
 * @package controllers/api/base
 */

namespace API\Controllers\Base;

use Entities\ResponsePayload;
use Phalcon\Mvc\Dispatcher;

abstract class Controller extends \Phalcon\Mvc\Controller {

	/** @var ResponsePayload $api_response Our custom response object that is used for all API returns */
	public $api_response = '';

	/**
	 * Runs specific logic before every found action
	 *
	 * @param Dispatcher $dispatcher
	 */
	public function beforeExecuteRoute(Dispatcher $dispatcher): void {
		$this->api_response = new ResponsePayload();
	}
}
